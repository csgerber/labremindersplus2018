package edu.uchicago.gerber.labreminderclassproject;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by ag on 5/1/2015.
 */
public class RemindersSimpleCursorAdapter extends SimpleCursorAdapter {

   private String  strColor;
   private int nColor;

  private HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();

    public RemindersSimpleCursorAdapter(Context context, int layout, Cursor c, String[]
            from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);


    }

    public void setColor() {
        strColor = PreferenceManager.getDefaultSharedPreferences(mContext).getString("highlight_color", "orange");
        switch (strColor){
            case "cyan":
                nColor = R.color.cyan;
                break;
            case "blue":
                nColor = R.color.blue;
                break;
            case "orange":
            default:
                nColor = R.color.orange;

                break;


        }
    }






    //to use a viewholder, you must override the following two methods and define a ViewHolder class
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return super.newView(context, cursor, parent);
    }
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        super.bindView(view, context, cursor);
        ViewHolder holder = (ViewHolder) view.getTag();
        if (holder == null) {
            holder = new ViewHolder();
            holder.colImp = cursor.getColumnIndexOrThrow(RemindersDbAdapter.COL_IMPORTANT);
            holder.listTab = view.findViewById(R.id.row_tab);
            view.setTag(holder);
        }if (cursor.getInt(holder.colImp) > 0) {
            holder.listTab.setBackgroundColor(context.getResources().getColor(nColor));
        } else {
            holder.listTab.setBackgroundColor(context.getResources().getColor(R.color.green));
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = super.getView(position, convertView, parent);//let the adapter handle setting up the row views
        v.setBackgroundColor(mContext.getResources().getColor(R.color.dark_grey)); //default color

        if (convertView != null) {
            TextView rowText = convertView.findViewById(R.id.row_text);
            rowText.setTextColor(mContext.getResources().getColor(android.R.color.white));

            if (mSelection.get(position) != null) {
                v.setBackgroundColor(mContext.getResources().getColor(R.color.cyan));
                rowText.setTextColor(mContext.getResources().getColor( android.R.color.black));
            }
        }


        return v;
    }


    public void setNewSelection(int position, boolean value) {
        mSelection.put(position, value);
        notifyDataSetChanged();
    }

    public boolean isPositionChecked(int position) {
        Boolean result = mSelection.get(position);
        return result == null ? false : result;
    }

    public Set<Integer> getCurrentCheckedPosition() {
        return mSelection.keySet();
    }

    public void removeSelection(int position) {
        mSelection.remove(position);
        notifyDataSetChanged();
    }

    public void clearSelection() {
        mSelection = new HashMap<Integer, Boolean>();
        notifyDataSetChanged();
    }



    static class ViewHolder {
        //store the column index
        int colImp;
        //store the view
        View listTab;
    }
}